import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:komptech_live/data/bloc/TasksBloc.dart';
import 'package:komptech_live/data/bloc/bloc-data.dart';
import 'package:komptech_live/data/models/job.dart';
import 'package:komptech_live/ui/tasks_list/ListItem.dart';

class TasksList extends StatelessWidget {
  final TasksBloc tasksBloc;

  TasksList({Key key, @required this.tasksBloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BlocData<List<Job>>>(
        initialData: BlocData.loading(),
        stream: tasksBloc.jobsStream,
        builder: (context, builder) {
          BlocData data;
          if (!builder.hasData) {
            return Text("Error loading data");
          }
          data = builder.data;

          if (data.isLoading) {
            return Center(child: CircularProgressIndicator.adaptive());
          }
          if (data.isError) {
            return Text("Error: ${data.error}");
          }
          List<Job> jobs = data.data;

          return RefreshIndicator(
            onRefresh: tasksBloc.loadJobs,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return JobListItem(job: jobs[index]);
              },
              itemCount: jobs.length,
            ),
          );
        });
  }
}
