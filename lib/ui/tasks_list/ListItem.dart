import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:komptech_live/data/models/job.dart';
import 'package:komptech_live/data/models/material.dart';
import 'package:komptech_live/di/di.dart';
import 'package:komptech_live/ui/task_detail/TaskDetail.dart';

class JobListItem extends StatelessWidget {
  final Job job;
  final df = DateFormat("EEE, HH:mm");

  JobListItem({Key key, @required this.job}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text("${job.client?.firstname} ${job.client?.lastname} #${job.id}", style: job.done ? TextStyle(color: Colors.grey) : null),
      subtitle: Text("${df.format(job.date)}, Musterstraße 4\n${job.material.toShortString()}", style: job.done ? TextStyle(color: Colors.grey) : null),
      leading: Icon(getIcon()),
      isThreeLine: true,
      trailing: Icon(Icons.chevron_right),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return TaskDetail(taskId: job.id, tasksBloc: DependencyInjection.tasksBloc);
        }));
      },
    );
  }

  IconData getIcon() {
    if (!job.hasWorkStarted) {
      return Icons.person;
    } else if (job.hasWorkStarted && !job.done) {
      return Icons.engineering;
    } else {
      return Icons.done;
    }
  }
}