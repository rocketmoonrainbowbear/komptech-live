import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:komptech_live/data/bloc/TasksBloc.dart';
import 'package:komptech_live/data/bloc/bloc-data.dart';
import 'package:komptech_live/data/models/job.dart';
import 'package:komptech_live/data/models/material.dart';
import 'package:komptech_live/ui/task_detail/Counter.dart';
import 'package:komptech_live/ui/task_detail/Gauges.dart';
import 'package:komptech_live/ui/task_detail/PdfDetail.dart';
import 'package:url_launcher/url_launcher.dart';

class TaskDetail extends StatefulWidget {
  final int taskId;
  final TasksBloc tasksBloc;

  const TaskDetail({
    Key key,
    @required this.taskId,
    @required this.tasksBloc,
  }) : super(key: key);

  @override
  _TaskDetailState createState() => _TaskDetailState();
}

class _TaskDetailState extends State<TaskDetail> {
  Future<BlocData<Job>> jobFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Job detail")),
      body: FutureBuilder<BlocData<Job>>(
        future: jobFuture,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator.adaptive());
          }

          if (!snapshot.data.hasData) {
            return Center(child: Text(snapshot.data.error));
          }

          final job = snapshot.data.data;
          final client = job.client;
          return ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 24, left: 16, bottom: 16),
                child: Row(
                  children: [
                    Text("Client: ", style: TextStyle(fontSize: 24)),
                    Text("${client.firstname} ${client.lastname}",
                        style: TextStyle(fontSize: 24)),
                  ],
                ),
              ),
              ListTile(
                leading: Icon(Icons.engineering_rounded),
                title: Text(job.material.toShortString()),
              ),
              ListTile(
                leading: Icon(Icons.phone_rounded),
                title: Text(client.phonenumber),
                trailing: Icon(Icons.chevron_right),
                onTap: () {
                  launch("tel:${job.client.phonenumber.replaceAll(" ", "")}");
                },
              ),
              ListTile(
                leading: Icon(Icons.mail_rounded),
                title: Text(client.email),
                trailing: Icon(Icons.chevron_right),
                onTap: () {
                  launch("mailto:${job.client.email}");
                },
              ),
              Divider(),
              ...workNotStartedWidgets(job),
              workStartedWidgets(job),
              workFinishedWidgets(job),
            ],
          );
        },
      ),
    );
  }

  Widget workFinishedWidgets(Job job) {
    if (!job.done) {
      return Container();
    }

    return Column(
      children: [
        Text(
          "Summary",
          style: TextStyle(fontSize: 24),
        ),
        Image.asset("assets/img/machine.png"),
        ListTile(
          leading: Icon(Icons.eco_outlined),
          title: Text("Application"),
          subtitle: Text("Green waste"),
        ),
        ListTile(
          leading: Icon(Icons.engineering_outlined),
          title: Text("Total troughput"),
          subtitle: Text("37.8 t"),
        ),
        ListTile(
          leading: Icon(Icons.local_gas_station_rounded),
          title: Text("Used fuel"),
          subtitle: Text("64.8 l"),
        ),
        ListTile(
          leading: Icon(Icons.battery_charging_full_rounded),
          title: Text("Used battery"),
          subtitle: Text("16 %"),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 24,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton.icon(
                icon: Icon(Icons.file_copy_outlined),
                onPressed: exportPdf,
                label: Text("Export as PDF"),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget workStartedWidgets(Job job) {
    if (!job.hasWorkStarted || job.done) {
      return Container();
    }

    return Column(
      children: [
        Text(
          "Multistar L3",
          style: TextStyle(fontSize: 24),
        ),
        Image.asset("assets/img/machine.png"),
        Text("Time spent"),
        Counter(start: job.workStarted),
        Divider(),
        Gauges(throughput: 27.0, fuelConsumption: 40.0),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 24,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton.icon(
                icon: Icon(Icons.stop_circle_outlined),
                onPressed: stopWork,
                label: Text("Stop work"),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> workNotStartedWidgets(Job job) {
    if (job.hasWorkStarted) {
      return [];
    }

    return [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Text("Details", style: TextStyle(fontSize: 24)),
      ),
      ListTile(
        leading: Icon(Icons.pin_drop_rounded),
        title: Text("Musterstraße 4, 8010 Graz"),
        subtitle: Text("Navigate"),
        trailing: Icon(Icons.chevron_right),
        onTap: () {
          if (Platform.isIOS) {
            launch("https://maps.apple.com/?q=${job.location.latitude},${job.location.longitude}");
          } else {
            launch("https://www.google.com/maps/search/?api=1&query=${job.location.latitude},${job.location.longitude}");
          }
        },
      ),
      Container(
        height: 200,
        child: GoogleMap(
          initialCameraPosition: CameraPosition(
            target: LatLng(job.location.latitude, job.location.longitude),
            zoom: 14,
          ),
          markers: {
            Marker(
              markerId: MarkerId(""),
              position: LatLng(job.location.latitude, job.location.longitude),
            ),
          },
          myLocationButtonEnabled: false,
          rotateGesturesEnabled: false,
          scrollGesturesEnabled: false,
          tiltGesturesEnabled: false,
          zoomControlsEnabled: false,
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 16.0, left: 16, right: 16),
        child: Text(
          "Multistar L3",
          style: TextStyle(fontSize: 24),
        ),
      ),
      Image.asset("assets/img/machine.png"),
      Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 24,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton.icon(
              icon: Icon(Icons.not_started_outlined),
              onPressed: startWork,
              label: Text("Start work"),
            ),
          ],
        ),
      ),
    ];
  }

  exportPdf() {
    Navigator.push(context, MaterialPageRoute(builder: (_) => PdfDetail()));
  }

  startWork() {
    widget.tasksBloc.startJob(widget.taskId);
    setState(() {
      jobFuture = widget.tasksBloc.getJob(widget.taskId);
    });
  }

  stopWork() {
    widget.tasksBloc.stopWork(widget.taskId);
    setState(() {
      jobFuture = widget.tasksBloc.getJob(widget.taskId);
    });
  }

  @override
  void initState() {
    super.initState();

    jobFuture = widget.tasksBloc.getJob(widget.taskId);
  }
}
