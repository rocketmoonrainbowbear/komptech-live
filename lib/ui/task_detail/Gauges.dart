import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class Gauges extends StatefulWidget {
  final double throughput;
  final double fuelConsumption;

  const Gauges({
    Key key,
    @required this.throughput,
    @required this.fuelConsumption,
  }) : super(key: key);

  @override
  _GaugesState createState() => _GaugesState();
}

class _GaugesState extends State<Gauges> {
  Timer _timer;
  Random _random = Random();

  double _throughput = 0;
  double _fuelConsumed = 0;
  double _deltaThroughput = 0;
  double _deltaFuelConsumption = 0;
  
  double _initialFuelLevel = 142.0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: Icon(Icons.engineering_rounded),
          title: Text("Total throughput"),
          subtitle: Text("${_throughput.toStringAsFixed(1)} t"),
        ),
        ListTile(
          leading: Icon(Icons.local_gas_station_rounded),
          title: Text("Fuel consumed"),
          subtitle: Text("${_fuelConsumed.toStringAsFixed(1)} l"),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 200,
              width: 200,
              child: SfRadialGauge(
                title: GaugeTitle(text: "Throughput"),
                enableLoadingAnimation: true,
                axes: [
                  RadialAxis(
                    minimum: 0,
                    maximum: 50,
                    ranges: <GaugeRange>[
                      GaugeRange(
                          startValue: 0, endValue: 30, color: Colors.green),
                      GaugeRange(
                          startValue: 30, endValue: 40, color: Colors.orange),
                      GaugeRange(
                          startValue: 40, endValue: 50, color: Colors.red),
                    ],
                    pointers: <GaugePointer>[
                      NeedlePointer(value: _deltaThroughput),
                    ],
                    annotations: <GaugeAnnotation>[
                      GaugeAnnotation(
                        widget: Container(
                          child: Text(
                            '${_deltaThroughput.toStringAsFixed(1)}\nt/h',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        angle: 90,
                        positionFactor: 0.5,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 200,
              width: 200,
              child: SfRadialGauge(
                title: GaugeTitle(text: "Fuel consumption"),
                enableLoadingAnimation: true,
                axes: [
                  RadialAxis(
                    minimum: 20,
                    maximum: 60,
                    pointers: <GaugePointer>[
                      NeedlePointer(value: _deltaFuelConsumption),
                    ],
                    annotations: <GaugeAnnotation>[
                      GaugeAnnotation(
                        widget: Container(
                          child: Text(
                            '${_deltaFuelConsumption.toStringAsFixed(1)}\nl/h',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        angle: 90,
                        positionFactor: 0.5,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        Text("Battery level (%)"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SfLinearGauge(
            minimum: 0,
            maximum: 100,
            ranges: [
              LinearGaugeRange(
                startValue: 0,
                endValue: 15,
                color: Colors.red,
              ),
              LinearGaugeRange(
                startValue: 15,
                endValue: 30,
                color: Colors.orange,
              ),
              LinearGaugeRange(
                startValue: 30,
                endValue: 100,
                color: Colors.green,
              ),
            ],
            markerPointers: [
              LinearShapePointer(value: 48),
            ],
          ),
        ),
        Text("Fuel (l)"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SfLinearGauge(
            minimum: 0,
            maximum: 200,
            ranges: [
              LinearGaugeRange(
                startValue: 0,
                endValue: 30,
                color: Colors.red,
              ),
              LinearGaugeRange(
                startValue: 30,
                endValue: 60,
                color: Colors.orange,
              ),
              LinearGaugeRange(
                startValue: 60,
                endValue: 200,
                color: Colors.green,
              ),
            ],
            markerPointers: [
              LinearShapePointer(value: _initialFuelLevel - _fuelConsumed),
            ],
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (_) {
      setState(() {
        _deltaThroughput = widget.throughput + _random.nextDouble();
        _deltaFuelConsumption = widget.fuelConsumption + _random.nextDouble();
        _throughput += _deltaThroughput / 3600;
        _fuelConsumed += _deltaFuelConsumption / 3600;
      });
    });
  }
}