import 'dart:async';
import 'dart:math';

import 'package:flutter/widgets.dart';

class Counter extends StatefulWidget {
  final DateTime start;

  const Counter({Key key, @required this.start}) : super(key: key);

  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  Timer _timer;

  @override
  Widget build(BuildContext context) {
    int seconds = max(DateTime.now().difference(widget.start).inSeconds, 0);
    int minutes = seconds ~/ 60;
    int hours = minutes ~/ 60;

    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String hoursStr = hours.toString().padLeft(2, '0');

    return Text(
      "$hoursStr:$minutesStr:$secondsStr",
      style: TextStyle(fontSize: 32),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (_) {
      setState(() {});
    });
  }
}
