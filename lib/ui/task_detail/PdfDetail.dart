import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:komptech_live/data/pdf/PdfExporter.dart';
import 'package:printing/printing.dart';

class PdfDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PDF Export"),
      ),
      body: PdfPreview(
        build: (_) async => (await PdfExporter.exportAsPdf()).save(),
      ),
    );
  }
}
