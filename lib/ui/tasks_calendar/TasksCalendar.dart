import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:komptech_live/data/bloc/TasksBloc.dart';
import 'package:komptech_live/ui/tasks_list/ListItem.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:komptech_live/data/bloc/bloc-data.dart';
import 'package:komptech_live/data/models/job.dart';

class TasksCalendar extends StatefulWidget {
  final TasksBloc tasksBloc;

  const TasksCalendar({Key key, @required this.tasksBloc}) : super(key: key);

  @override
  _TasksCalendarState createState() => _TasksCalendarState();
}

class _TasksCalendarState extends State<TasksCalendar> {
  ValueNotifier<List<Job>> _selectedJobs;

  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay;
  List<Job> _jobs;

  @override
  void initState() {
    super.initState();

    _selectedDay = _focusedDay;
    _selectedJobs = ValueNotifier(_getEventsForDay(_selectedDay));
  }

  @override
  void dispose() {
    _selectedJobs.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    if (!isSameDay(_selectedDay, selectedDay)) {
      setState(() {
        _selectedDay = selectedDay;
        _focusedDay = focusedDay;
      });

      _selectedJobs.value = _getEventsForDay(selectedDay);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BlocData<List<Job>>>(
      initialData: BlocData.loading(),
      stream: widget.tasksBloc.jobsStream,
      builder: (context, builder) {
        BlocData data;
        if (!builder.hasData) {
          return Text("Error loading data");
        }
        data = builder.data;

        if (data.isLoading) {
          return CircularProgressIndicator.adaptive();
        }
        if (data.isError) {
          return Text("Error: ${data.error}");
        }
        _jobs = data.data;
        _selectedJobs.value = _getEventsForDay(_focusedDay);

        return Column(children: [
          TableCalendar(
            calendarStyle: CalendarStyle(
              selectedDecoration: BoxDecoration(
                color: Color(0xFF008136),
                shape: BoxShape.circle,
              ),
              todayDecoration: BoxDecoration(
                color: Color(0x9300AA00),
                shape: BoxShape.circle,
              ),
            ),
            eventLoader: (day) {
              return _getEventsForDay(day);
            },
            firstDay: DateTime.now(),
            lastDay: DateTime.now().add(new Duration(days: 3650)),
            focusedDay: _focusedDay,
            calendarFormat: _calendarFormat,
            selectedDayPredicate: (day) {
              // Use `selectedDayPredicate` to determine which day is currently selected.
              // If this returns true, then `day` will be marked as selected.

              // Using `isSameDay` is recommended to disregard
              // the time-part of compared DateTime objects.
              return isSameDay(_selectedDay, day);
            },
            onDaySelected: _onDaySelected,
            onFormatChanged: (format) {
              if (_calendarFormat != format) {
                // Call `setState()` when updating calendar format
                setState(() {
                  _calendarFormat = format;
                });
              }
            },
            onPageChanged: (focusedDay) {
              // No need to call `setState()` here
              _focusedDay = focusedDay;
            },
          ),
          const SizedBox(height: 8.0),
          Expanded(
              child: ValueListenableBuilder<List<Job>>(
            valueListenable: _selectedJobs,
            builder: (context, value, _) {
              return ListView.builder(
                itemCount: value.length,
                itemBuilder: (context, index) {
                  final job = _getEventsForDay(_selectedDay)[index];
                  return JobListItem(job: job);
                },
              );
            },
          )),
        ]);
      },
    );
  }

  List<Job> _getEventsForDay(DateTime day) {
    if (_jobs == null) return List.empty();
    return _jobs.where((job) => isSameDay(job.date, day)).toList();
  }
}
