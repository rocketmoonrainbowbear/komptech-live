import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:komptech_live/data/bloc/TasksBloc.dart';
import 'package:komptech_live/data/models/job.dart';
import 'package:komptech_live/ui/task_detail/TaskDetail.dart';

class TasksMap extends StatefulWidget {
  final df = DateFormat("EEE, HH:mm");
  final TasksBloc tasksBloc;

  TasksMap({Key key, @required this.tasksBloc}) : super(key: key);

  @override
  _TasksMapState createState() => _TasksMapState();
}

class _TasksMapState extends State<TasksMap> {
  Completer<GoogleMapController> _controller = Completer();

  final cameraPosition = CameraPosition(
    target: LatLng(47.1129092, 15.2394294),
    zoom: 8,
  );

  final mapKey = GlobalKey();

  StreamSubscription _subscription;

  Set<Marker> _markers = {};
  Set<Polyline> _polyLines = {};

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      key: mapKey,
      initialCameraPosition: cameraPosition,
      onMapCreated: (controller) => _controller.complete(controller),
      markers: _markers,
      polylines: _polyLines,
    );
  }

  void addToMap(List<Job> jobs) async {
    setState(() {
      _markers = jobs
          .map((job) => Marker(
                markerId: MarkerId("${job.id}"),
                position: LatLng(job.location.latitude, job.location.longitude),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                  job.done
                      ? BitmapDescriptor.hueGreen
                      : (job.hasWorkStarted
                          ? BitmapDescriptor.hueBlue
                          : BitmapDescriptor.hueRed),
                ),
                infoWindow: InfoWindow(
                  title:
                      "${job.client?.firstname} ${job.client?.lastname} #${job.id}",
                  snippet: "${widget.df.format(job.date)}, Musterstraße 4",
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return TaskDetail(
                          taskId: job.id, tasksBloc: widget.tasksBloc);
                    }));
                  },
                ),
              ))
          .toSet();
      _polyLines.clear();
      jobs.sort((lhs, rhs) => lhs.id - rhs.id);
      final latLngs = jobs
          .where((element) => !element.done)
          .map((job) => LatLng(job.location.latitude, job.location.longitude))
          .toList();
      _polyLines.add(Polyline(
        polylineId: PolylineId(""),
        visible: true,
        points: latLngs,
        color: Colors.blue,
        width: 5,
      ));
    });
  }

  @override
  void initState() {
    super.initState();
    _subscription = widget.tasksBloc.jobsStream.listen((event) {
      if (!event.hasData) {
        return;
      }

      addToMap(event.data);
    });
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
