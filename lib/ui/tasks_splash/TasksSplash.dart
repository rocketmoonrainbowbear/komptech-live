import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:komptech_live/data/bloc/LoginBloc.dart';
import 'package:splash_screen_view/SplashScreenView.dart';
import 'package:komptech_live/data/bloc/SplashBloc.dart';
import 'package:komptech_live/ui/login/LoginPage.dart';

class TasksSplash extends StatefulWidget {
  final SplashBloc splashBloc;
  final LoginBloc loginBloc;

  const TasksSplash(
      {Key key, @required this.splashBloc, @required this.loginBloc})
      : super(key: key);

  @override
  _TasksSplashState createState() => _TasksSplashState();
}

class _TasksSplashState extends State<TasksSplash> {
  Widget _newPage;

  @override
  Widget build(BuildContext context) {
    this.getData();
    return Scaffold(
      body: _newPage == null
          ? Center(child: CircularProgressIndicator.adaptive())
          : SplashScreenView(
              home: _newPage,
              duration: 3000,
              imageSize: 100,
              imageSrc: "assets/img/icon.png",
              text: "Komptech Live",
              textType: TextType.TyperAnimatedText,
              textStyle: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
                color: Colors.black87,
              ),
              backgroundColor: Colors.white,
            ),
    );
  }

  Future<void> getData() async {
    // await widget.splashBloc.getDataFiles();
    // if (await widget.loginBloc.isLoggedIn()) {
    //   _newPage = HomePage();
    // } else {
    _newPage = LoginPage();
    // }

    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getData();
  }
}
