import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:komptech_live/di/di.dart';
import 'package:komptech_live/ui/tasks_add/TasksAdd.dart';
import 'package:komptech_live/ui/tasks_calendar/TasksCalendar.dart';
import 'package:komptech_live/ui/tasks_list/TasksList.dart';
import 'package:komptech_live/ui/tasks_map/TasksMap.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedPage = 0;

  final tasksBloc = DependencyInjection.tasksBloc;

  final children = <Widget>[
    TasksList(tasksBloc: DependencyInjection.tasksBloc),
    TasksCalendar(tasksBloc: DependencyInjection.tasksBloc),
    TasksMap(tasksBloc: DependencyInjection.tasksBloc),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Komptech Live"),
        actions: [
          IconButton(
            icon: StreamBuilder<bool>(
                stream: tasksBloc.showDoneJobsStream,
                builder: (context, snapshot) {
                  return Icon(snapshot.data ?? false ? Icons.timer : Icons.done);
                }),
            onPressed: tasksBloc.toggleShowDoneJobs,
          ),
        ],
      ),
      body: children[_selectedPage],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today_rounded),
            label: "Calendar",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            label: "Map",
          ),
        ],
        currentIndex: _selectedPage,
        onTap: (int index) {
          setState(() {
            _selectedPage = index;
          });
        },
      ),
      floatingActionButton: _selectedPage == 2
          ? null
          : FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => TasksAdd(
                      DateTime.now(),
                    ),
                  ),
                );
              },
              child: Icon(Icons.add),
              backgroundColor: Colors.green,
            ),
    );
  }

  @override
  void initState() {
    super.initState();
    DependencyInjection.tasksBloc.loadJobs();
  }
}
