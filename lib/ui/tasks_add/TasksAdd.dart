import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:select_form_field/select_form_field.dart';

class TasksAdd extends StatelessWidget {
  DateTime _dateTime = DateTime.now();

  TasksAdd(this._dateTime);

  final List<Map<String, dynamic>> _applications = [
    {
      'value': 'waste_wood',
      'label': 'Waste wood',
    },
    {
      'value': 'green_waste',
      'label': 'Green waste',
    },
    {
      'value': 'root_stocks',
      'label': 'Root stocks',
    },
    {
      'value': 'other',
      'label': 'Other',
    },
  ];

  final List<Map<String, dynamic>> _items = [
    {
      'value': 'machine_1',
      'label': 'Machine 1',
    },
    {
      'value': 'machine_2',
      'label': 'Machine 2',
    },
    {
      'value': 'machine_3',
      'label': 'Machine 3',
    },
    {
      'value': 'machine_4',
      'label': 'Machine 4',
    },
    {
      'value': 'machine_5',
      'label': 'Machine 5',
    },
    {
      'value': 'machine_6',
      'label': 'Machine 6',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Add Job")),
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
          child: Form(
            child: ListView(
              children: <Widget>[
                InputDatePickerFormField(
                  fieldLabelText: "Date",
                  initialDate: _dateTime,
                  firstDate: DateTime.now(),
                  lastDate: DateTime.now().add(Duration(days: 3650)),
                ),
                const SizedBox(height: 8.0),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Customer',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 8.0),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Company',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 8.0),
                TextFormField(
                  keyboardType: TextInputType.numberWithOptions(),
                  decoration: InputDecoration(
                    labelText: 'Address',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 8.0),
                SelectFormField(
                  type: SelectFormFieldType.dropdown, // or can be dialog
                  labelText: 'Machine',
                  items: _items,
                ),
                const SizedBox(height: 8.0),
                SelectFormField(
                  type: SelectFormFieldType.dropdown, // or can be dialog
                  labelText: 'Applications',
                  items: _applications,
                ),
                const SizedBox(height: 8.0),
                TextFormField(
                  keyboardType: TextInputType.numberWithOptions(),
                  decoration: InputDecoration(
                    labelText: 'Amount',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 8.0),
                TextFormField(
                  minLines: 3,
                  maxLines: 3,
                  decoration: InputDecoration(
                    labelText: 'Comments',
                    border: OutlineInputBorder(),
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (_) => TasksAdd(DateTime.now())));
          },
          child: Icon(Icons.save),
          backgroundColor: Colors.green,
        ));
  }
}
