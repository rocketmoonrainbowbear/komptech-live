import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:komptech_live/data/bloc/LoginBloc.dart';
import 'package:komptech_live/di/di.dart';
import 'package:komptech_live/ui/home/HomePage.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState(DependencyInjection.loginBloc);
}

class _LoginPageState extends State<LoginPage> {
  final LoginBloc loginBloc;

  _LoginPageState(this.loginBloc);

  bool loading = false;

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Login")),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 150,
            padding: EdgeInsets.all(40),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(200)),
            child: Center(
              child: Image.asset("assets/img/logo.png"),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'E-Mail',
                  hintText: 'Enter your company E-Mail'),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  hintText: 'Enter your password'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                OutlinedButton(
                  onPressed: loading
                      ? null
                      : () {
                          _login();
                        },
                  child: Text('Login'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _login() async {
    setState(() {
      loading = true;
    });
    if (await loginBloc.login(
      username: usernameController.text,
      password: usernameController.text,
    )) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (_) => HomePage()));
      return;
    }
    setState(() {
      loading = false;
    });
  }
}
