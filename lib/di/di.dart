import 'package:komptech_live/data/bloc/LoginBloc.dart';
import 'package:komptech_live/data/bloc/SplashBloc.dart';
import 'package:komptech_live/data/bloc/TasksBloc.dart';

class DependencyInjection {
  static TasksBloc _tasksBloc;

  static TasksBloc get tasksBloc {
    if (_tasksBloc == null) {
      _tasksBloc = new TasksBloc();
    }
    return _tasksBloc;
  }

  static SplashBloc _splashBloc;

  static SplashBloc get splashBloc  {
    if (_splashBloc == null) {
      _splashBloc = new SplashBloc();
    }
    return _splashBloc;
  }

  static LoginBloc _loginBloc;

  static LoginBloc get loginBloc  {
    if (_loginBloc == null) {
      _loginBloc = new LoginBloc();
    }
    return _loginBloc;
  }
}
