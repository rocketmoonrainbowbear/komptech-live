import 'package:flutter/material.dart';
import 'package:komptech_live/di/di.dart';
import 'package:komptech_live/ui/tasks_splash/TasksSplash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Komptech Live',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        primaryColor: Color.fromRGBO(0, 129, 54, 1),
      ),
      home: TasksSplash(
        splashBloc: DependencyInjection.splashBloc,
        loginBloc: DependencyInjection.loginBloc,
      ),
    );
  }
}
