import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc {

  Future<bool> login({username: String, password: String}) async {
    final prefs = await SharedPreferences.getInstance();

    await Future.delayed(Duration(seconds: 1));

    await prefs.setBool("logged_in", true);

    return true;
  }

  Future<bool> isLoggedIn() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool("logged_in") ?? false;
  }
}
