import 'dart:async';

import 'package:komptech_live/data/bloc/bloc-data.dart';
import 'package:komptech_live/data/models/job.dart';
import 'package:komptech_live/data/models/location.dart';
import 'package:komptech_live/data/models/material.dart';
import 'package:komptech_live/data/models/person.dart';
import 'package:rxdart/rxdart.dart';

class TasksBloc {
  final _jobsSubject = BehaviorSubject<BlocData<List<Job>>>();
  final _showDoneJobs = BehaviorSubject<bool>();

  ValueStream<BlocData<List<Job>>> get jobsStream => _jobsSubject.stream;

  ValueStream<bool> get showDoneJobsStream => _showDoneJobs.stream;

  TasksBloc() {
    _showDoneJobs.add(false);
  }

  Future<void> loadJobs({bool forceLoad = true}) async {
    if (forceLoad) {
      await Future.delayed(Duration(seconds: 1));
    }

    final comp = (Job lhs, Job rhs) => lhs.date.compareTo(rhs.date);
    final jobsDone = this.jobs.where((element) => element.done).toList();
    final workingDone = this.jobs.where((element) => element.hasWorkStarted && !element.done).toList();
    final others = this.jobs.where((element) => !element.hasWorkStarted && !element.done).toList();
    jobsDone.sort(comp);
    workingDone.sort(comp);
    others.sort(comp);

    final jobs = [
      ...workingDone,
      ...others,
      ...jobsDone,
    ];

    _jobsSubject.add(BlocData.data(jobs.where((element) => !showDoneJobs || element.done == showDoneJobs).toList()));
  }

  List<Job> jobs = [
    Job(
      id: 1,
      client: Person(
        firstname: "Nikola",
        lastname: "Tesla",
        phonenumber: "+43 316 8730",
        email: "nikola.tesla@student.tugraz.at",
      ),
      date: DateTime.now().add(Duration(days: 3, hours: 3)),
      material: ProcessMaterial.Other,
      location: Location(47.06, 15.45),
    ),
    Job(
      id: 1257,
      client: Person(
        firstname: "Jana",
        lastname: "Türlich",
        phonenumber: "+39 123 456 7890",
        email: "jana.türlich@gmail.com",
      ),
      date: DateTime.now(),
      material: ProcessMaterial.GreenWaste,
      location: Location(47.1129092, 15.2394294),
    ),
    Job(
      id: 1261,
      client: Person(
        firstname: "Lenz",
        lastname: "Lustig",
        phonenumber: "+39 123 456 7890",
        email: "lustigerlenz12@yahoo.com",
      ),
      date: DateTime.now().add(Duration(hours: 2)),
      material: ProcessMaterial.RootStocks,
      location: Location(47.2, 15.4),
    ),
    Job(
      id: 1255,
      client: Person(
        firstname: "Simon",
        lastname: "Schabernack",
        phonenumber: "+39 123 456 7890",
        email: "simons@gmail.com",
      ),
      date: DateTime.now().add(Duration(days: 1)),
      material: ProcessMaterial.WasteWood,
      location: Location(47.0, 15.3),
    ),
    Job(
      id: 1258,
      client: Person(
        firstname: "Rainer",
        lastname: "Zufall",
        phonenumber: "+39 123 456 7890",
        email: "rainer@zufall.com",
      ),
      date: DateTime.now().add(Duration(days: 1, hours: 3)),
      material: ProcessMaterial.GreenWaste,
      location: Location(47.16, 15.25),
    ),
    Job(
      id: 12217,
      client: Person(
        firstname: "Wilma",
        lastname: "Bier",
        phonenumber: "+43 123 456 7890",
        email: "wilma@🍺.com",
      ),
      date: DateTime.now().add(Duration(days: 4, hours: 3)),
      material: ProcessMaterial.WasteWood,
      location: Location(47.2, 14.65),
    ),
    Job(
      id: 1646,
      client: Person(
        firstname: "Hella",
        lastname: "Wahnsinn",
        phonenumber: "+59 321 456 7890",
        email: "hella@crazy.com",
      ),
      date: DateTime.now().add(Duration(days: 5, hours: 3)),
      material: ProcessMaterial.GreenWaste,
      location: Location(47.4, 15.2),
    ),
    Job(
      id: 1645,
      client: Person(
        firstname: "Paul",
        lastname: "Ahner",
        phonenumber: "+49 089 456 7890",
        email: "paul@paulaner.com",
      ),
      date: DateTime.now().add(Duration(days: 3, hours: 3)),
      material: ProcessMaterial.GreenWaste,
      location: Location(47.2, 15.1),
    ),
  ];

  bool showDoneJobs = false;

  Future<BlocData<Job>> getJob(int id) async {
    final job = jobs.firstWhere((element) => element.id == id);

    if (job == null) {
      return BlocData.error("Job does not exist");
    } else {
      return BlocData.data(job);
    }
  }

  Future<void> startJob(int id) async {
    final job = jobs.firstWhere((element) => element.id == id);

    if (job != null) {
      job.workStarted = DateTime.now();
      loadJobs(forceLoad: false);
    }
  }

  Future<void> stopWork(int id) async {
    final job = jobs.firstWhere((element) => element.id == id);

    if (job != null) {
      job.done = true;
      job.workEnded = DateTime.now();
      loadJobs(forceLoad: false);
    }
  }

  void toggleShowDoneJobs() {
    showDoneJobs = !showDoneJobs;
    _showDoneJobs.add(showDoneJobs);
    loadJobs(forceLoad: false);
  }
}
