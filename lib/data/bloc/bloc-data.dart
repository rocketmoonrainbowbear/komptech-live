class BlocData<T> {
  final BlocDataType _type;
  final T _data;
  final String _error;

  BlocData._(this._type, this._data, this._error)
      : assert(_type != null),
        assert((_type == BlocDataType.Loading &&
                _data == null &&
                _error == null) ||
            (_type == BlocDataType.Error && _error != null && _data == null) ||
            (_type == BlocDataType.Data && _data != null && _error == null));

  BlocData.loading() : this._(BlocDataType.Loading, null, null);

  BlocData.data(T data) : this._(BlocDataType.Data, data, null);

  BlocData.error(String _error) : this._(BlocDataType.Error, null, _error);

  T get data {
    assert(_type == BlocDataType.Data);
    return _data;
  }

  String get error {
    assert(_type == BlocDataType.Error);
    return _error;
  }

  bool get isLoading => _type == BlocDataType.Loading;

  bool get hasData => _type == BlocDataType.Data;

  bool get isError => _type == BlocDataType.Error;
}

enum BlocDataType {
  Loading,
  Error,
  Data,
}
