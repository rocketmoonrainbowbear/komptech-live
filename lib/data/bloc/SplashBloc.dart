import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:komptech_live/data/bloc/bloc-data.dart';
import 'package:komptech_live/data/models/machine.dart';
import 'package:rxdart/rxdart.dart';

class SplashBloc {
  final _machinesDataSubject = BehaviorSubject<BlocData<List<Machine>>>();

  ValueStream<BlocData<List<Machine>>> get machineDataStream => _machinesDataSubject.stream;

  Future<void> getDataFiles() async {
    final dataFiles = await rootBundle.loadString("assets/json/manifest.json");
    final dataJson = jsonDecode(dataFiles);
    final list = [];
    for (var data in dataJson) {
        var str = await rootBundle.loadString(data);
        final machine = Machine.fromJson(json.decode(str));
        list.add(machine);
    }
    _machinesDataSubject.add(BlocData.data(list));
  }

}

