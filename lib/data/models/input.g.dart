// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'input.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Input _$InputFromJson(Map<String, dynamic> json) {
  return Input(
    json['name'] as String,
    json['bytebit'] as String,
    json['canid'] as int,
    json['value'] as String,
    json['bytenr'] as int,
    json['xmlchannel'] as int,
  );
}

Map<String, dynamic> _$InputToJson(Input instance) => <String, dynamic>{
      'name': instance.name,
      'bytebit': instance.bytebit,
      'canid': instance.canid,
      'value': instance.value,
      'bytenr': instance.bytenr,
      'xmlchannel': instance.xmlchannel,
    };
