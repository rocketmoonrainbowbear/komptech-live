import 'package:komptech_live/data/models/machine.dart';
import 'package:komptech_live/data/models/material.dart';
import 'package:komptech_live/data/models/person.dart';

import 'location.dart';

class Job {
  int id;
  Person contractor;
  Person client;
  Location location;
  Machine machine;
  DateTime date;
  DateTime workStarted;
  DateTime workEnded;
  double fuelConsumption;
  double energyEfficiency;
  double price;
  bool payed;
  bool signed;
  bool done;
  ProcessMaterial material;

  Job({
    this.id,
    this.contractor,
    this.client,
    this.location,
    this.machine,
    this.date,
    this.workStarted,
    this.workEnded,
    this.fuelConsumption,
    this.energyEfficiency,
    this.price,
    this.payed = false,
    this.signed = false,
    this.done = false,
    this.material,
  });

  bool get hasWorkStarted => workStarted != null;

  Duration get duration => workEnded.difference(workStarted);
}
