// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sensor _$SensorFromJson(Map<String, dynamic> json) {
  return Sensor(
    json['name'] as String,
    json['handle'] as String,
    (json['factor'] as num)?.toDouble(),
    json['unit'] as String,
    json['bytenr'] as int,
    json['xmlchannel'] as int,
    json['extrainfo'] as String,
  );
}

Map<String, dynamic> _$SensorToJson(Sensor instance) => <String, dynamic>{
      'name': instance.name,
      'handle': instance.handle,
      'factor': instance.factor,
      'unit': instance.unit,
      'bytenr': instance.bytenr,
      'xmlchannel': instance.xmlchannel,
      'extrainfo': instance.extrainfo,
    };
