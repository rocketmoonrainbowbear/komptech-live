import 'package:json_annotation/json_annotation.dart';

part 'sensor.g.dart';

@JsonSerializable()
class Sensor {
  Sensor(
    this.name,
    this.handle,
    this.factor,
    this.unit,
    this.bytenr,
    this.xmlchannel,
    this.extrainfo,
  );

  String name;
  String handle;
  double factor;
  String unit;
  int bytenr;
  int xmlchannel;
  String extrainfo;

  factory Sensor.fromJson(Map<String, dynamic> json) => _$SensorFromJson(json);

  Map<String, dynamic> toJson() => _$SensorToJson(this);
}
