import 'package:json_annotation/json_annotation.dart';
import 'package:komptech_live/data/models/input.dart';
import 'package:komptech_live/data/models/sensor.dart';
import 'package:komptech_live/data/models/error.dart';

part 'machine.g.dart';

@JsonSerializable(explicitToJson: true)
class Machine {
  Machine(
    this.name,
    this.sensors,
    this.inputs,
    this.errors,
  );

  String name;
  List<Sensor> sensors;
  List<Input> inputs;
  List<Error> errors;

  factory Machine.fromJson(Map<String, dynamic> json) =>
      _$MachineFromJson(json);

  Map<String, dynamic> toJson() => _$MachineToJson(this);
}
