// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'machine.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Machine _$MachineFromJson(Map<String, dynamic> json) {
  return Machine(
    json['name'] as String,
    (json['sensors'] as List)
        ?.map((e) =>
            e == null ? null : Sensor.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['inputs'] as List)
        ?.map(
            (e) => e == null ? null : Input.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['errors'] as List)
        ?.map(
            (e) => e == null ? null : Error.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$MachineToJson(Machine instance) => <String, dynamic>{
      'name': instance.name,
      'sensors': instance.sensors?.map((e) => e?.toJson())?.toList(),
      'inputs': instance.inputs?.map((e) => e?.toJson())?.toList(),
      'errors': instance.errors?.map((e) => e?.toJson())?.toList(),
    };
