import 'package:json_annotation/json_annotation.dart';

part 'input.g.dart';

@JsonSerializable()
class Input {
  Input(
    this.name,
    this.bytebit,
    this.canid,
    this.value,
    this.bytenr,
    this.xmlchannel,
  );

  String name;
  String bytebit;
  int canid;
  String value;
  int bytenr;
  int xmlchannel;

  factory Input.fromJson(Map<String, dynamic> json) => _$InputFromJson(json);

  Map<String, dynamic> toJson() => _$InputToJson(this);
}
