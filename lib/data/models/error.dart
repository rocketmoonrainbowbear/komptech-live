import 'package:json_annotation/json_annotation.dart';

part 'error.g.dart';

@JsonSerializable()
class Error {
  Error(
    this.name,
    this.id,
    this.bytebit,
    this.canid,
    this.bytenr,
    this.xmlchannel,
    this.messages,
  );

  String name;
  int id;
  double bytebit;
  int canid;
  int bytenr;
  int xmlchannel;
  List<String> messages;

  factory Error.fromJson(Map<String, dynamic> json) => _$ErrorFromJson(json);

  Map<String, dynamic> toJson() => _$ErrorToJson(this);
}
