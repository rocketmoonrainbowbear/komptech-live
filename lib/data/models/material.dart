enum ProcessMaterial {
  WasteWood,
  GreenWaste,
  RootStocks,
  Other,
}

extension ParseToString on ProcessMaterial {
  String toShortString() {
    switch (this) {
      case ProcessMaterial.WasteWood:
        return "Waste Wood";
      case ProcessMaterial.GreenWaste:
        return "Green Waste";
      case ProcessMaterial.RootStocks:
        return "Root Stocks";
      case ProcessMaterial.Other:
        return "Other";
    }
    // should never happen
    return "Other";
  }
}
