// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Error _$ErrorFromJson(Map<String, dynamic> json) {
  return Error(
    json['name'] as String,
    json['id'] as int,
    (json['bytebit'] as num)?.toDouble(),
    json['canid'] as int,
    json['bytenr'] as int,
    json['xmlchannel'] as int,
    (json['messages'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'bytebit': instance.bytebit,
      'canid': instance.canid,
      'bytenr': instance.bytenr,
      'xmlchannel': instance.xmlchannel,
      'messages': instance.messages,
    };
