import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:flutter/services.dart' show rootBundle;

class PdfExporter {
  static Future<pw.Document> exportAsPdf() async {
    final pdf = pw.Document();
    final bytes = await rootBundle.load("assets/img/machine.png");

    pdf.addPage(
      pw.Page(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return pw.Column(
            crossAxisAlignment: pw.CrossAxisAlignment.start,
            children: [
              pw.Text("Invoice", style: pw.TextStyle(fontSize: 32)),
              pw.Divider(),
              pw.Text("Client: Max Mustermann", style: pw.TextStyle(fontSize: 16)),
              pw.Text("Phone number: +39 123 456 7890", style: pw.TextStyle(fontSize: 16)),
              pw.Text("E-Mail address: info@example.com", style: pw.TextStyle(fontSize: 16)),
              pw.Text("Address: Musterstraße 4, 8010 Graz", style: pw.TextStyle(fontSize: 16)),
              pw.Divider(),
              pw.Text("Processed material: Green Waste", style: pw.TextStyle(fontSize: 16)),
              pw.Text("Amount processed: 37.8 t", style: pw.TextStyle(fontSize: 16)),
              pw.Text("Hours billed: 1h 40m", style: pw.TextStyle(fontSize: 16)),
              pw.Text("Fuel billed: 64.8 l", style: pw.TextStyle(fontSize: 16)),
              pw.Divider(),
              pw.Text("Machine: Multistar L3", style: pw.TextStyle(fontSize: 24)),
              pw.Center(
                child: pw.Image(
                  pw.MemoryImage(bytes.buffer.asUint8List()),
                ),
              ),
            ],
          );
        },
      ),
    );

    return pdf;
    // await Printing.layoutPdf(onLayout: (format) => pdf.save());
  }
}
